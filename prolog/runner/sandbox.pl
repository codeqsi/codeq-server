:- multifile sandbox:safe_primitive/1.

% needed for call_with_inference_limit/3
sandbox:safe_primitive(system:'$inference_limit'(_,_)).
sandbox:safe_primitive(system:'$inference_limit_except'(_,_,_)).
sandbox:safe_primitive(system:'$inference_limit_false'(_)).
sandbox:safe_primitive(system:'$inference_limit_true'(_,_,_)).

% needed for algol problems
% TODO find a better way
sandbox:safe_primitive(call(_)).

% safe_primitive declarations for library(clpr) (by Gašper Škulj)
sandbox:safe_primitive(bb_r:bb_inf(_,_,_)).
sandbox:safe_primitive(bb_r:bb_inf(_,_,_,_,_)).
sandbox:safe_primitive(bb_r:vertex_value(_,_)).

sandbox:safe_primitive(nf_r:{}(_)).
sandbox:safe_primitive(nf_r:nf(_,_)).
sandbox:safe_primitive(nf_r:entailed(_)).
sandbox:safe_primitive(nf_r:split(_,_,_)).
sandbox:safe_primitive(nf_r:repair(_,_)).
sandbox:safe_primitive(nf_r:nf_constant(_,_)).
sandbox:safe_primitive(nf_r:nf2term(_,_)).
sandbox:safe_primitive(nf_r:resubmit_eq(_)).
sandbox:safe_primitive(nf_r:submit_eq(_)).
sandbox:safe_primitive(nf_r:submit_eq_b(_)).
sandbox:safe_primitive(itf_r:do_checks(_,_,_,_,_,_,_,_)).

sandbox:safe_primitive(ineq_r:ineq(_,_,_,_)).
sandbox:safe_primitive(ineq_r:ineq_one(_,_,_,_)).
sandbox:safe_primitive(ineq_r:ineq_one_n_n_0(_)).
sandbox:safe_primitive(ineq_r:ineq_one_n_p_0(_)).
sandbox:safe_primitive(ineq_r:ineq_one_s_n_0(_)).
sandbox:safe_primitive(ineq_r:ineq_one_s_p_0(_)).

sandbox:safe_primitive(bb_r:bb_inf(_,_,_)).
sandbox:safe_primitive(bb_r:bb_inf(_,_,_,_,_)).
sandbox:safe_primitive(bb_r:vertex_value(_,_)).

sandbox:safe_primitive(bv_r:allvars(_,_)).
sandbox:safe_primitive(bv_r:backsubst(_,_,_)).
sandbox:safe_primitive(bv_r:backsubst_delta(_,_,_,_)).
sandbox:safe_primitive(bv_r:basis_add(_,_)).
sandbox:safe_primitive(bv_r:dec_step(_,_)).
sandbox:safe_primitive(bv_r:deref(_,_)).
sandbox:safe_primitive(bv_r:deref_var(_,_)).
sandbox:safe_primitive(bv_r:detach_bounds(_)).
sandbox:safe_primitive(bv_r:detach_bounds_vlv(_,_,_,_,_)).
sandbox:safe_primitive(bv_r:determine_active_dec(_)).
sandbox:safe_primitive(bv_r:determine_active_inc(_)).
sandbox:safe_primitive(bv_r:dump_var(_,_,_,_,_,_)).
sandbox:safe_primitive(bv_r:dump_nz(_,_,_,_,_)).
sandbox:safe_primitive(bv_r:export_binding(_)).
sandbox:safe_primitive(bv_r:export_binding(_,_)).
sandbox:safe_primitive(bv_r:get_or_add_class(_,_)).
sandbox:safe_primitive(bv_r:inc_step(_,_)).
sandbox:safe_primitive(bv_r:intro_at(_,_,_)).
sandbox:safe_primitive(bv_r:iterate_dec(_,_)).
sandbox:safe_primitive(bv_r:lb(_,_,_)).
sandbox:safe_primitive(bv_r:pivot_a(_,_,_,_)).
sandbox:safe_primitive(bv_r:pivot(_,_,_,_,_)).
sandbox:safe_primitive(bv_r:rcbl_status(_,_,_,_,_,_)).
sandbox:safe_primitive(bv_r:reconsider(_)).
sandbox:safe_primitive(bv_r:same_class(_,_)).
sandbox:safe_primitive(bv_r:solve(_)).
sandbox:safe_primitive(bv_r:solve_ord_x(_,_,_)).
sandbox:safe_primitive(bv_r:ub(_,_,_)).
sandbox:safe_primitive(bv_r:unconstrained(_,_,_,_)).
sandbox:safe_primitive(bv_r:var_intern(_,_)).
sandbox:safe_primitive(bv_r:var_intern(_,_,_)).
sandbox:safe_primitive(bv_r:var_with_def_assign(_,_)).
sandbox:safe_primitive(bv_r:var_with_def_intern(_,_,_,_)).
sandbox:safe_primitive(bv_r:maximize(_)).
sandbox:safe_primitive(bv_r:minimize(_)).
sandbox:safe_primitive(bv_r:sup(_,_)).
sandbox:safe_primitive(bv_r:sup(_,_,_,_)).
sandbox:safe_primitive(bv_r:inf(_,_)).
sandbox:safe_primitive(bv_r:inf(_,_,_,_)).
sandbox:safe_primitive(bv_r:'solve_<'(_)).
sandbox:safe_primitive(bv_r:'solve_=<'(_)).
sandbox:safe_primitive(bv_r:'solve_=\\='(_)).
sandbox:safe_primitive(bv_r:log_deref(_,_,_,_)).
sandbox:safe_primitive(fourmotz_r:fm_elim(_,_,_)).

sandbox:safe_primitive(store_r:add_linear_11(_,_,_)).
sandbox:safe_primitive(store_r:add_linear_f1(_,_,_,_)).
sandbox:safe_primitive(store_r:add_linear_ff(_,_,_,_,_)).
sandbox:safe_primitive(store_r:normalize_scalar(_,_)).
sandbox:safe_primitive(store_r:delete_factor(_,_,_,_)).
sandbox:safe_primitive(store_r:mult_linear_factor(_,_,_)).
sandbox:safe_primitive(store_r:nf_rhs_x(_,_,_,_)).
sandbox:safe_primitive(store_r:indep(_,_)).
sandbox:safe_primitive(store_r:isolate(_,_,_)).
sandbox:safe_primitive(store_r:nf_substitute(_,_,_,_)).
sandbox:safe_primitive(store_r:mult_hom(_,_,_)).
sandbox:safe_primitive(store_r:nf2sum(_,_,_)).
sandbox:safe_primitive(store_r:nf_coeff_of(_,_,_)).
sandbox:safe_primitive(store_r:renormalize(_,_)).

sandbox:safe_primitive(itf:dump_linear(_,_,_)).
sandbox:safe_primitive(itf:dump_nonzero(_,_,_)).
sandbox:safe_primitive(itf:clp_type(_,_)).
sandbox:safe_primitive(itf:attr_unify_hook(_,_)).

sandbox:safe_primitive(class:class_allvars(_,_)).
sandbox:safe_primitive(class:class_new(_,_,_,_,_)).
sandbox:safe_primitive(class:class_drop(_,_)).
sandbox:safe_primitive(class:class_basis(_,_)).
sandbox:safe_primitive(class:class_basis_add(_,_,_)).
sandbox:safe_primitive(class:class_basis_drop(_,_)).
sandbox:safe_primitive(class:class_basis_pivot(_,_,_)).
sandbox:safe_primitive(class:class_get_clp(_,_)).
sandbox:safe_primitive(class:class_get_prio(_,_)).
sandbox:safe_primitive(class:class_put_prio(_,_)).

sandbox:safe_primitive(geler:geler(_,_,_)).
sandbox:safe_primitive(geler:project_nonlin(_,_,_)).
sandbox:safe_primitive(geler:collect_nonlin(_,_,_)).
sandbox:safe_primitive(geler:attach(_,_,_)).

sandbox:safe_primitive(dump:dump(_,_,_)).

sandbox:safe_primitive(ordering:combine(_,_,_)).
sandbox:safe_primitive(ordering:ordering(_)).
sandbox:safe_primitive(ordering:arrangement(_,_)).

sandbox:safe_primitive(project:drop_dep(_)).
sandbox:safe_primitive(project:drop_dep_one(_)).
sandbox:safe_primitive(project:make_target_indep(_,_)).
sandbox:safe_primitive(project:project_attributes(_,_)).

sandbox:safe_primitive(redund:redundancy_vars(_)).
sandbox:safe_primitive(redund:systems(_,_,_)).

sandbox:safe_primitive(ugraphs:add_edges(_,_,_)).
sandbox:safe_primitive(ugraphs:add_vertices(_,_,_)).
sandbox:safe_primitive(ugraphs:complement(_,_,_)).
sandbox:safe_primitive(ugraphs:compose(_,_,_)).
sandbox:safe_primitive(ugraphs:del_edges(_,_,_)).
sandbox:safe_primitive(ugraphs:del_vertices(_,_,_)).
sandbox:safe_primitive(ugraphs:vertices_edges_to_ugraph(_,_,_)).
sandbox:safe_primitive(ugraphs:ugraph_union(_,_,_)).
