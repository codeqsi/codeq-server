#!/usr/bin/swipl -q -s

/* CodeQ: an online programming tutor.
   Copyright (C) 2015 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

% Pengine and HTTP server modules.
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_error)).
:- use_module(library(http/thread_httpd)).
:- use_module(library(pengines)).
:- use_module(library(pengines_io)).

:- use_module(library(clpfd)).
:- use_module(library(clpr)).

:- use_module(library(lists)).

:- consult(sandbox).

:- multifile prolog:error_message/3.
prolog:error_message(time_limit_exceeded) -->
  [ 'time limit exceeded' ].

:- set_setting(pengine_sandbox:time_limit, 5.0).
:- set_setting(pengine_sandbox:thread_pool_size, 500).

% Load required predicates then disable autoloading.
:- autoload([verbose(false)]).
:- set_prolog_flag(autoload, false).

% Start the server.
:- http_server(http_dispatch,
                [port(3030),
                 workers(10),
                 timeout(30),
                 keep_alive_timeout(30)]).
:- writeln('Prolog engine started.').
