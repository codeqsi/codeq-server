#!/usr/bin/swipl -q --nosignals -tty -s

/* CodeQ: an online programming tutor.
   Copyright (C) 2015 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

% Pengine and HTTP server modules.
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_error)).
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_unix_daemon)).
:- use_module(library(pengines)).
:- use_module(library(pengines_io)).

:- use_module(library(clpfd)).
:- use_module(library(clpr)).

:- use_module(library(lists)).

:- consult(sandbox).

:- multifile prolog:error_message/3.
prolog:error_message(time_limit_exceeded) -->
  [ 'time limit exceeded' ].

:- set_setting(pengine_sandbox:time_limit, 5.0).
:- set_setting(pengine_sandbox:thread_pool_size, 500).

start_server :-
    Common = [fork(false), workers(10), timeout(30), keep_alive_timeout(30), user(nobody), group(nogroup)],
    % SWI-Prolog 7.3.22 changed the options for specifying server address
    current_prolog_flag(version, Version),
    (Version >= 70322 ->
        Options = [http(localhost:3030) | Common]
        ;
        Options = [ip(localhost), port(3030) | Common]
    ),
    http_daemon(Options).

:- start_server.
