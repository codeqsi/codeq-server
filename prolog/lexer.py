#!/usr/bin/python3

# CodeQ: an online programming tutor.
# Copyright (C) 2015-2017 UL FRI
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import ply.lex as lex

operators = {
    r':-': 'FROM',
    r'-->': 'FROMDCG',
    r',': 'COMMA',
    r';': 'SEMI',
    r'->': 'IMPLIES',
    r'*->': 'SOFTCUT',
    r'\+': 'NOT',
    r'=': 'EQU',
    r'\=': 'NEQU',
    r'=@=': 'EQV',
    r'\=@=': 'NEQV',
    r'==': 'EQ',
    r'\==': 'NEQ',
    r'=..': 'UNIV',
    r'is': 'IS',
    r'=:=': 'EQA',
    r'=\=': 'NEQA',
    r'<': 'LT',
    r'=<': 'LE',
    r'>': 'GT',
    r'>=': 'GE',
    r'@<': 'LTL',
    r'@=<': 'LEL',
    r'@>': 'GTL',
    r'@>=': 'GEL',
    r'+': 'PLUS',
    r'-': 'MINUS',
    r'*': 'STAR',
    r'/': 'SLASH',
    r'//': 'SLASH2',
    r'<<': 'SHIFTLEFT',
    r'>>': 'SHIFTRIGHT',
    r'div': 'DIV',
    r'mod': 'MOD',
    r'rdiv': 'RDIV',
    r'rem': 'REM',
    r'xor': 'XOR',
    r'^': 'POW',
    r'**': 'POWSTAR',
    '/\\': 'AND',
    '\\': 'NEG',

    # CLP(FD)
    r'in': 'IN',
    r'ins': 'INS',
    r'..': 'THROUGH',
    r'#=': 'FDEQ',
    r'#\=': 'FDNEQ',
    r'#<': 'FDLT',
    r'#=<': 'FDLE',
    r'#>': 'FDGT',
    r'#>=': 'FDGE',
    r'\/': 'FDUNION',
}
tokens = sorted(list(operators.values())) + [
    'UINTEGER', 'UREAL',
    'NAME', 'VARIABLE', 'STRING',
    'LBRACKET', 'RBRACKET', 'LPAREN', 'RPAREN', 'PIPE', 'LBRACE', 'RBRACE',
    'PERIOD', 'INVALID'
]

# punctuation
t_LBRACKET = r'\['
t_RBRACKET = r'\]'
t_LPAREN = r'\('
t_RPAREN = r'\)'
t_PIPE = r'\|'
t_LBRACE = r'{'
t_RBRACE = r'}'

t_UINTEGER = r'[0-9]+'
t_UREAL    = r'[0-9]+\.[0-9]+([eE][-+]?[0-9]+)?|inf|nan'
t_VARIABLE = r'(_|[A-Z])[a-zA-Z0-9_]*'
t_STRING   = r'"(""|\\.|[^\"])*"'

# TODO support nested comments
def t_comment(t):
    r'(/\*(.|\n)*?\*/)|(%.*)'
    pass

def t_NAME(t):
    r"'(''|\\.|[^\\'])*'|[a-z][a-zA-Z0-9_]*|[-+*/\\^<>=~:.?@#$&]+|!|;|,"
    if t.value == '.':
        t.type = 'PERIOD'
    else:
        # return appropriate tokens for names that are operators
        t.type = operators.get(t.value, 'NAME')
    return t

t_ignore  = ' \t'

def t_newline(t):
    r'\n+'
    t.lexer.lineno += len(t.value)

def t_error(t):
    # TODO send this to stderr
    #print("Illegal character '" + t.value[0] + "'")
    t.type = 'INVALID'
    t.value = t.value[0]
    t.lexer.skip(1)
    return t

lexer = lex.lex(errorlog=lex.NullLogger())

if __name__ == '__main__':
    while True:
        try:
            s = input('> ')
        except EOFError:
            break
        if not s:
            continue

        lexer.input(s)
        tokens = list(lexer)
        print(tokens)
