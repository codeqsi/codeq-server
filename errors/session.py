# CodeQ: an online programming tutor.
# Copyright (C) 2015 UL FRI
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

class NoSuchSession(Exception):
    pass

class AuthenticationFailed(Exception):
    pass

class UserExists(Exception):
    pass

class SignupFailed(Exception):
    pass

class PasswordChangeFailed(Exception):
    pass

class RequestProcessingError(Exception):
    pass

class NotLoggedIn(Exception):
    pass

class AccountMergeRequired(Exception):

    def __init__(self, username):
        Exception.__init__(self)
        self.username = username  # the username which is required to be logged in, in order to merge the accounts
