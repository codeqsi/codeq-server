# CodeQ: an online programming tutor.
# Copyright (C) 2015 UL FRI
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Hint.instantiate takes a hint object (dictionary) to be sent to the client
# and sets additional fields based on previously sent hints.

# Simple hint: don't do anything on instantiation.
class Hint(object):
    hint_type = 'static'

    def __init__(self, name):
        self.name = name

    def instantiate(self, hint, prev_hints):
        pass

# Popup hints: don't do anything on instantiation.
class HintPopup(Hint):
    hint_type = 'popup'

    def __init__(self, name, style=None):
        self.name = name
        self.style = style

    def instantiate(self, hint, prev_hints):
        if self.style is not None:
            hint['style'] = self.style
