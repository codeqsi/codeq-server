# CodeQ: an online programming tutor.
# Copyright (C) 2015 UL FRI
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

__all__ = ['socket', 'handlers', 'user_session', 'prolog_session', 'python_session', 'problems', 'LanguageSession']

# language session handlers are added as their modules are loaded and processed
language_session_handlers = {}

# the base class for all language session handlers
class LanguageSession(object):

    def destroy(self):
        pass

    def hint(self, sid, problem_id, program):
        pass

    def test(self, sid, problem_id, program):
        pass

# these imports must be made after LanguageSession is defined, otherwise they won't succeed
# the order of imports is important! first language sessions, then user_session

import server.prolog_session
import server.python_session
import server.user_session
import server.socket
import server.problems
import server.handlers

