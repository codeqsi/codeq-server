#!/usr/bin/python3

# CodeQ: an online programming tutor.
# Copyright (C) 2015 UL FRI
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

if __name__ == '__main__':
    import os
    import logging
    logging.basicConfig(filename=os.environ.get('CODEQ_SERVER_LOG') or 'codeq-server.log', level=logging.INFO, format='%(asctime)s %(levelname)s %(message)s')

    import signal
    signal.signal(signal.SIGCHLD, signal.SIG_IGN)

    import multiprocessing
    multiprocessing.set_start_method('forkserver')

    import server
    server.socket.serve_forever()
