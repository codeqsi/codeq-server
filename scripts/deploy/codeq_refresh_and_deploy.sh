#!/bin/dash -e

. /etc/default/codeq
export CODEQ_PROBLEMS CODEQ_WEB_OUTPUT CODEQ_DB_HOST CODEQ_DB_DATABASE CODEQ_DB_USER CODEQ_DB_PASS

# flags to restart daemons at the end of the script
# 0 means not to restart, anything else means to restart
RESTART_PROLOG=0
RESTART_SERVER=0
RESTART_WEB=0
BUILD_WEB_RESOURCES=0

init=$(cat /proc/1/comm)

cd $CODEQ_SERVER
git fetch -q origin $CODEQ_GIT_BRANCH
git checkout -q $CODEQ_GIT_BRANCH
FILES=$(mktemp)
if git diff --name-status origin/$CODEQ_GIT_BRANCH | cut -c3- | grep -v \\.gitignore >$FILES; then
    # some modifications happened
    git merge -q

    if grep -q ^prolog/runner/ $FILES; then
        RESTART_PROLOG=1
    fi

    if grep -qv '^\(web\|prolog/runner\|scripts\)/' $FILES; then
        RESTART_SERVER=1
    fi

    if grep -q ^web/ $FILES; then
        RESTART_WEB=1
    fi

    if grep -q ^scripts/build_web_resources.py $FILES; then
        BUILD_WEB_RESOURCES=1
    fi

    # rebuild sandbox if out of date
    for prog in python/runner/sandbox; do
        if ! make -q "${prog}"; then
            make "${prog}" && setcap cap_setuid,cap_setgid+ep "${prog}"
        fi
    done

    if grep ^web/ $FILES | grep -qv ^web/main.js; then
        # node dependencies may have changed, run installation
        cd $CODEQ_SERVER/web && /usr/bin/npm install
    fi
fi

cd $CODEQ_PROBLEMS
git fetch -q origin $CODEQ_GIT_BRANCH
git checkout -q $CODEQ_GIT_BRANCH
if git diff --name-status origin/$CODEQ_GIT_BRANCH | cut -c3- | grep -v \\.gitignore >$FILES; then
    git merge -q
    BUILD_WEB_RESOURCES=1
    # restart the server if any other files than language files were modified
    if grep -q -v '\(^\|/\)..\.py' $FILES; then
        RESTART_SERVER=1
    fi
fi

cd $CODEQ_WEB
git fetch -q origin $CODEQ_GIT_BRANCH
git checkout -q $CODEQ_GIT_BRANCH
if git diff --name-status origin/$CODEQ_GIT_BRANCH | cut -c3- | grep -v \\.gitignore >$FILES; then
    git merge -q
    echo Redeploying web app
    /usr/bin/rsync -aq --delete -f '- .git' -f '- .gitignore' -f '- config.xml' -f 'P data' /var/local/codeq-web/ /var/www/html
fi

rm -f $FILES

# as a precaution switch to the tmp folder
cd /tmp
if [ $BUILD_WEB_RESOURCES -ne 0 ]; then
    echo Rebuilding web resources
    /usr/bin/python3 $CODEQ_SERVER/scripts/build_web_resources.py
fi
if [ $RESTART_PROLOG -ne 0 ]; then
    echo Restarting codeq-prolog
    if [ "$init" = systemd ]; then
        systemctl restart codeq-prolog
    else
        /etc/init.d/codeq-prolog restart
    fi
fi
if [ $RESTART_SERVER -ne 0 ]; then
    echo Restarting codeq-server
    if [ "$init" = systemd ]; then
        systemctl restart codeq-server
    else
        /etc/init.d/codeq-server restart
    fi
fi
if [ $RESTART_WEB -ne 0 ]; then
    echo Restarting codeq-web
    if [ "$init" = systemd ]; then
        systemctl restart codeq-web
    else
        /etc/init.d/codeq-web restart
    fi
fi

exit 0
