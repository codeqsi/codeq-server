#!/usr/bin/python

# CodeQ: an online programming tutor.
# Copyright (C) 2015 UL FRI
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys

from server.problems import load_problem

if len(sys.argv) < 5:
    sys.stderr.write('usage: {} programming_language group problem language\n'.format(sys.argv[0]))
    sys.exit(1)
language, group, problem, tail = sys.argv[1:]
mod = load_problem(language, group, problem, tail)

print('''\
<DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>Problem</title>
    <style>
h2 {
    margin-top: 1em;
    margin-bottom: 0.5em;
}
h3 {
    margin-bottom: 0.3em;
    margin-top: 0.2em;
}
p {
    margin-bottom: 0.2em;
    margin-top: 0;
}
section {
    margin-bottom: 1.5em;
}
    </style>
  </head>
  <body>''')

print('<h1>{}</h1>'.format(mod.name))
print('{}'.format(mod.description))

print('<section>')
print('<h2>Plan</h2>')
for n, plan in enumerate(mod.plan):
    print('<section>')
    print('<h3>Plan #{}</h3>'.format(n))
    if isinstance(plan, list):
        for i, p_part in enumerate(plan):
            if isinstance(p_part, dict):
                p_part = p_part['message']
            print(p_part)
            if i < len(plan)-1:
                print('<hr />')
    else:
        print(plan)
    print('</section>')
print('</section>')

print('<section>')
print('<h2>Hints</h2>')
for id, hint in sorted(mod.hint.items()):
    print('<section>')
    print('<h3>{}</h3>'.format(id))
    if isinstance(hint, list):
        for i, hint_part in enumerate(hint):
            if isinstance(hint_part, dict):
                hint_part = hint_part['message']
            print(hint_part)
            if i < len(hint)-1:
                print('<hr />')
    else:
        print(hint)
    print('</section>')
print('</section>')

print('''\
  </body>
</html>''')
