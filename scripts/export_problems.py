#!/usr/bin/python3

# CodeQ: an online programming tutor.
# Copyright (C) 2015 UL FRI
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys, sqlite3, os, os.path
from .utils import filenamefy

base_dir = '/tmp' # where to create the directory structure

if not os.path.isdir(base_dir):
    os.mkdir(base_dir)

prolog_dir = base_dir + '/prolog'
prolog_facts_dir = prolog_dir + '/facts'
prolog_groups_dir = prolog_dir + '/problems'
if not os.path.isdir(prolog_dir):
    os.mkdir(prolog_dir)
if not os.path.isdir(prolog_facts_dir):
    os.mkdir(prolog_facts_dir)
if not os.path.isdir(prolog_groups_dir):
    os.mkdir(prolog_groups_dir)

def log(line):
    sys.stdout.write('{0}\n'.format(line))

fact_map = {}
group_map = {}

sqc = sqlite3.connect('db.sqlite3') # sqlite3 connection handle
try:
    cur = sqc.cursor()
    try:
        for row in cur.execute('select id, name, facts from tutor_library'):
            facts_module = filenamefy(row[1])
            facts_filename = prolog_facts_dir + '/' + facts_module + '.py'
            fact_map[row[0]] = facts_module
            f = open(facts_filename, 'w', encoding='UTF-8', errors='replace', newline='')
            try:
                f.write("""\
# coding=utf-8

id = {0}

name = '{1}'

facts = '''\\
{2}'''
""".format(row[0], row[1], '\n'.join(row[2].split('\r\n'))))
            finally:
                f.close()
            log('facts module: {0}'.format(facts_filename))

        for row in cur.execute('select id, name from tutor_group'):
            group_dir = filenamefy(row[1])
            group_dir_abs = prolog_groups_dir + '/' + group_dir
            if not os.path.isdir(group_dir_abs):
                os.mkdir(group_dir_abs)
            group_map[row[0]] = group_dir
            log('group dir: {0}'.format(group_dir_abs))

        for row in cur.execute('select id, group_id, number, name, slug, details, solution, visible, library_id from tutor_problem'):
            group_dir = group_map[row[1]]
            name = row[3]
            problem_dir = filenamefy(name)
            problem_dir_abs = prolog_groups_dir + '/' + group_dir + '/' + problem_dir # contains all python modules for the problem
            if not os.path.isdir(problem_dir_abs):
                os.mkdir(problem_dir_abs)
            facts_module = None if row[8] is None else fact_map.get(row[8])
            visible = row[7]
            f = open(problem_dir_abs + '/common.py', 'w', encoding='UTF-8', errors='replace', newline='')
            try:
                f.write("""\
# coding=utf-8

id = {0}
group = '{1}'
number = {2}
visible = {3}
facts = {4}

solution = '''\\
{5}'''
""".format(row[0], group_dir, row[2], 'False' if visible == 0 else 'True', 'None' if facts_module is None else "'" + facts_module + "'", '\n'.join(row[6].split('\r\n'))))
            finally:
                f.close()
            f = open(problem_dir_abs + '/en.py', 'w', encoding='UTF-8', errors='replace', newline='')
            try:
                f.write("""\
# coding=utf-8

id = {0}
name = '{1}'
slug = '{2}'

description = '''\\
{3}'''

hint = {{}}
""".format(row[0], name, row[4], '\n'.join(row[5].split('\r\n'))))
            finally:
                f.close()
            log('problem: {0}'.format(problem_dir_abs))
    finally:
        cur.close()
finally:
    sqc.close()
