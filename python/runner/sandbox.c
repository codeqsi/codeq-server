/* CodeQ: an online programming tutor.
   Copyright (C) 2015 UL FRI

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Affero General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <pwd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <unistd.h>

int main(int argc, char* argv[])
{
	if (argc < 3) {
		fprintf(stderr, "usage: %s USERNAME FILE [ARGS...]\n", argv[0]);
		return 1;
	}

	// initialize arguments for the sandboxed command
	char** args = malloc((argc-1) * sizeof(char*));
	int i;
	for (i = 0; i < argc-2; i++)
		args[i] = argv[i+2];
	args[argc-2] = (char*)0;

	// switch user (requires root or "setcap cap_setuid,cap_setgid+ep")
	char const* username = argv[1];
	struct passwd const* pw = getpwnam(username);
	if (!pw) {
		fprintf(stderr, "no such user: %s\n", username);
		return 1;
	}
	int ret = 0;
	if ((ret = setgid(pw->pw_gid)) != 0)
		fprintf(stderr, "setgid returned %d\n", ret);
	if ((ret = setuid(pw->pw_uid)) != 0)
		fprintf(stderr, "setuid returned %d\n", ret);

	// limit CPU time to 1 second
	struct rlimit const cpu_limit = { .rlim_cur = 1, .rlim_max = 1 };
	if ((ret = setrlimit(RLIMIT_CPU, &cpu_limit)) != 0)
		fprintf(stderr, "setrlimit(CPU) returned %d\n", ret);

	// don't allow writing files of any size
	struct rlimit const fsize_limit = { .rlim_cur = 0, .rlim_max = 0 };
	if ((ret = setrlimit(RLIMIT_FSIZE, &fsize_limit)) != 0)
		fprintf(stderr, "setrlimit(FSIZE) returned %d\n", ret);

	// there will be no fork
	struct rlimit const nproc_limit = { .rlim_cur = 0, .rlim_max = 0 };
	if ((ret = setrlimit(RLIMIT_NPROC, &nproc_limit)) != 0)
		fprintf(stderr, "setrlimit(NPROC) returned %d\n", ret);

	return execvp(args[0], args);
}
